# SDML - HW2 Task1

import os
import numpy as np
import pandas as pd
import collections
import csv
import logging

format_ = ('[%(asctime)s] {%(filename)s:%(lineno)d} '
           '%(levelname)s - %(message)s')
LOGGING_LEVEL = logging.INFO
logging.basicConfig(
    format=format_,
    level=LOGGING_LEVEL
)
logger = logging.getLogger(__name__)

# Read input data
logger.info("Reading input")

DATASET_PATH = 'data/'
food_csv = os.path.join(DATASET_PATH, 'food.csv')
food_df = pd.read_csv(food_csv)  # chmod 777 first
rating_csv = os.path.join(DATASET_PATH, 'rating_train.csv')
rating_df = pd.read_csv(rating_csv)  # chmod 777 first

# Build user-foods dict {userid: [foodid]}
logger.info("Building dict")

sorted_userID = sorted(list(set(rating_df['userid'])))
user_foods = collections.defaultdict(set)
for _, col in rating_df.iterrows():
    user_foods[col['userid']].add(col['foodid'])

# Find nearest neighbors w.r.t the number of common items
logger.info("Finding nn")

threshold = 48
user_neighbors = collections.defaultdict(list)  # {user_id: [neighbor_id]}
for user in sorted_userID:
    for candi in sorted_userID:
        if user != candi:
            num_common = len(
                np.intersect1d(list(user_foods[user]), list(user_foods[candi]))
            )
            if num_common > threshold:
                user_neighbors[user].append(candi)

# Find all food candidates corresponding to the neighbors of the user
logger.info("All food candidates")

user_candis = collections.defaultdict(list)
for user in sorted_userID:
    neighbors = user_neighbors[user]
    for neighbor in neighbors:
        candis = user_foods[neighbor]
        for candi in candis:
            user_candis[user].append(candi)

# Top K elements in Food ID

K = len(food_df)
food_id = list(rating_df['foodid'])
food_count = collections.Counter(food_id)
top_K_foods = [k[0] for k in food_count.most_common(K)]


# Write output
logger.info("Write output")

with open('output.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)

    # Header
    writer.writerow(['userid', 'foodid'])

    for user in sorted_userID:
        s = ""
        count = 0
        food_count = collections.Counter(user_candis[user])
        top_K_candis = [k[0] for k in food_count.most_common(len(food_df))]
        if len(top_K_candis) > 0:
            for food in top_K_candis:
                if food not in user_foods[user]:
                    s += str(food)
                    count += 1
                    if count < 20:
                        s += ' '
                    else:
                        break
        else:
            for food in top_K_foods:
                if food not in user_foods[user]:
                    s += str(food)
                    count += 1
                    if count < 20:
                        s += ' '
                    else:
                        break

        writer.writerow([user, s])
