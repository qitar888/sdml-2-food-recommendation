import sys
import pickle
from collections import defaultdict

from lightfm import LightFM
from lightfm.cross_validation import random_train_test_split
from lightfm.evaluation import precision_at_k
import numpy as np
import scipy.sparse as sparse
from scipy.sparse import coo_matrix

LOAD_MODEL = True

input_path = sys.argv[1]

data = sparse.load_npz(input_path)
# train, valid = random_train_test_split(data, test_percentage=0.1)
train = data

model = LightFM(loss='warp')
model.fit(train, epochs=100, num_threads=2)
model_file = open(model_path, "wb")

data_matrix = data.toarray()
negate = coo_matrix((data_matrix==0).astype(int))

pred = model.predict(negate.row, negate.col)
sorted_rows = defaultdict(list)

tuples = zip(negate.row, negate.col, pred.data)
for i in sorted(tuples, key=lambda x: (x[0], x[2])):
     sorted_rows[i[0]].append((i[1], i[2]))

# for every user id (row), print top 20
# print(sorted_rows)
