import os 
import numpy as np
import pandas as pd
from lightfm.data import Dataset
from lightfm import LightFM


DATASET_PATH = 'data/'
# food_csv = os.path.join(DATASET_PATH, 'food.csv')
# user_csv = os.path.join(DATASET_PATH, 'user.csv')
rating_csv = os.path.join(DATASET_PATH, 'rating_train.csv')

# chmod 777 first
# food_df = pd.read_csv(food_csv)
# user_df = pd.read_csv(user_csv)
rating_df = pd.read_csv(rating_csv)

# Build ID Mapping
dataset = Dataset()
dataset.fit((rating_df['userid']),(rating_df['foodid']))
num_users, num_items = dataset.interactions_shape()
print('Num users: {}, num_items {}.'.format(num_users, num_items))

# Build Intersections Matrix
(interactions, weights) = dataset.build_interactions(((col['userid'], col['foodid']) \
                                                   for _, col in rating_df.iterrows()))

print(repr(interactions))

# Build Model
model = LightFM(loss='bpr')
model.fit(interactions, epochs=100, num_threads=2, item_features=None)

