import sys

import numpy as np
import scipy.sparse as sparse
from scipy.sparse import coo_matrix


input_path = sys.argv[1]
output_path = sys.argv[2]

data = np.loadtxt(input_path, delimiter=",", dtype=str)[1:]
time = data[:,0]
rate = data[:,1:].astype(int)

max_user = np.max(rate[:,0])
max_item = np.max(rate[:,1])

# ijv format
sparse_matrix = coo_matrix((np.ones(rate.shape[0]),
                            (rate[:, 0], rate[:, 1])), shape=(max_user+1, max_item+1))

sparse.save_npz(output_path, sparse_matrix)
