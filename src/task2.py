# SDML - HW2 Task2

import os
import pandas as pd
import collections
import csv
import logging


format_ = ('[%(asctime)s] {%(filename)s:%(lineno)d} '
           '%(levelname)s - %(message)s')
LOGGING_LEVEL = logging.INFO
logging.basicConfig(
    format=format_,
    level=LOGGING_LEVEL
)
logger = logging.getLogger(__name__)

# Read input data
logger.info("Reading input")

DATASET_PATH = 'data/'
rating_csv = os.path.join(DATASET_PATH, 'rating_train.csv')
rating_df = pd.read_csv(rating_csv)  # chmod 777 first


# Build user-foods dict {user: [foodid]}
# Note: `foodid` can be repeated for each `userid`
logger.info("Building dict")

sorted_userID = sorted(list(set(rating_df['userid'])))
user_foods = collections.defaultdict(list)  # set -> list
for _, col in rating_df.iterrows():
    user_foods[col['userid']].append(col['foodid'])


# Write output
logger.info("Write output")

with open('output.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)

    # Header
    writer.writerow(['userid', 'foodid'])

    for user in sorted_userID:
        s = ""
        count = 0
        food_count = collections.Counter(user_foods[user])
        top_K_candis = [k[0] for k in food_count.most_common(20)]

        for food in top_K_candis:
            s += str(food)
            count += 1
            if count < 20:
                s += ' '
            else:
                break

        writer.writerow([user, s])
