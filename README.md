#STDML HW2 - Food Recommendation

## Installation
* Install pandas
`pip install pandas`

## Prepare data
1. Download data
2. Put all .csv to `data/` (We need 'data/food.csv' and 'data/rating_train.csv')
3. Change .csv mode to 777

## Run

### Run Task1.py
`python src/task1.py`

### Run Task2.py
`python src/task2.py`

## Others
### Convert rating.csv to sparse matrix for LightFM
`python src/rating_to_sparse_matrix.py data/{something}.csv data/{someotherthing}.npz`

### Follow lightfm instructions to build datasets
`python src/lightfm_build_dataset.py`


### Team

r07944019_張雅量

r07922006_林俊辰

b03902003_陳家陞
